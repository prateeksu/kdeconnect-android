package org.kde.kdeconnect.Helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class BootHelper {
    public static void setStartOnBootEnabled(Context context, boolean enabled) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putBoolean("startOnBoot", enabled).apply();
    }

    public static boolean isStartOnBootEnabled(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("startOnBoot", true);
    }

}
